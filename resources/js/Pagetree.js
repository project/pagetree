function Pagetree() {

  this.publishNode = function(nodeId, lang, message) {
    var entry = jQuery('.pt-entry-' + nodeId + '-' + lang);
    entry.addClass("pt-saving");
    Drupal.restconsumer
      .patch('/pagetree/publish', { id: nodeId, language: lang, message: message }, false)
      .done(
        (function (entry) {
          return function (e) {
            entry.find('.pt-state').removeClass('fa-exclamation-circle').removeClass('fa-times-circle').addClass('fa-check-circle');
            entry.removeClass('pt-saving').addClass('pt-saved');
            setTimeout(
              (function (entry) {
                return function () {
                  entry.removeClass('pt-saved');
                }
              })(entry)
              , 1500);
          }
        })(entry)
      )
      .fail(
        (function (entry) {
          return function (e) {
            entry.addClass('pt-error');
          }
        })(entry)
      );
  }

  this.unpublishNode = function(nodeId, lang, message) {
    var entry = jQuery('.pt-entry-' + nodeId + '-' + lang);
    entry.addClass("pt-saving");
    Drupal.restconsumer
      .patch('/pagetree/unpublish', { id: nodeId, language: lang, message: message })
      .done(
        (function (entry) {
          return function (e) {
            entry.find('.pt-state').removeClass('fa-check-circle').removeClass('fa-exclamation-circle').addClass('fa-times-circle');
            entry.removeClass('pt-saving').addClass('pt-saved');
            setTimeout(
              (function (entry) {
                return function () {
                  entry.removeClass('pt-saved');
                }
              })(entry)
              , 1500);
          }
        })(entry)
      )
      .fail(
        (function (entry) {
          return function (e) {
            entry.addClass('pt-error');
          }
        })(entry)
      );
  }
}
