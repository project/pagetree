
(function ($, Drupal) {
  var defaultLang = drupalSettings.pagetree.defaultLanguage;
  var currentLanguage = drupalSettings.path.currentLanguage;
  var currentNode = drupalSettings.path.nid;
  var openElements = {};
  var tree = null;
  var state = 'out';
  var timeout = null;
  openElements = JSON.parse(localStorage.getItem("pt-states"));
  if (openElements == null) {
    openElements = {};
  }
  var loading = false;

  setInterval(
    function () {
      if (state != 'hover' && !loading) {
        loading = true;
        var hash = sessionStorage.getItem("pt-tree-hash");
        Drupal.restconsumer.get('/pagetree/tree/' + hash).done(function (data) {
          if (typeof data.hash != 'undefined' && data.hash !== hash) {
            sessionStorage.setItem("pt-tree-hash", data.hash);
            createTree(data, currentNode);
          }
          loading = false;
        });
      }
    },
    5000
  );

  Drupal.behaviors.pagetreeInit = {
    attach: function (context, settings) {
      // Append pagetree
      let tree = $('.block-pagetree', context);
      $('body', context).append(tree);

      // Adjust offset if toolbar is present.
      var treeContainer = tree.find('.pt-pagetree-wrapper');
      if ($('#toolbar-administration').length > 0 && $('#toolbar-administration').is(':visible')) {
        treeContainer.addClass('toolbar-enabled');
      }

      // Initialize pagetree data.
      let loading = true;
      Drupal.restconsumer.get('/pagetree/tree/-')
        .done(function (data) {
          if (typeof data.hash == 'undefined') {
            data.hash = "-";
          }
          sessionStorage.setItem("pt-tree-hash", data.hash);
          createTree(data, currentNode);
          loading = false;
        }
        );

      // Initialize icon.
      var treeIcon = $('.pt-pagetree-icon');
      $(treeContainer).append(treeIcon);
      treeIcon.on("click", function () {
        treeContainer.toggleClass("pt-show");
      });

      // Add a shortcut to open/close the pagtree.
      $('body', context).on('keydown', function (e) {
        // Prevent execution when composing.
        if (e.isComposing || e.keyCode === 229) {
          return;
        }
        // Prevent execution in forms and editable elements.
        if (
          e.target.tagName == 'INPUT' ||
          e.target.tagName == 'TEXTAREA' ||
          e.target.contenteditable ||
          $(e.target).parents('form').length > 0) {
          return;
        }
        // React to "t" key, open/close the overlay.
        if (e.keyCode == 84) {
          treeContainer.toggleClass("pt-show");
        }
      });

      // Keep track of the mouse cursor being on the tree container.
      treeContainer.on('mouseover', function () {
        timeout = setTimeout(
          function () {
            state = 'hover';
          }, 100);
      });
      treeContainer.on('mouseout', function () {
        state = 'out';
        clearTimeout(timeout);
      });
    }
  };

  function createTree(data, currentNode) {
    $('.pt-trees-wrapper').empty();
    var openTree = localStorage.getItem("pt-open-tree");
    openTree = (typeof openTree !== 'undefined') ? openTree : data[0]['id'];
    var trees = data.trees;
    for (var x in trees) {
      if (trees.length > 1) {
        var treeContainer = $('<div class="pt-tree-container"><div class="pt-menu-label"><i class="pt-caret-icon fa fa-caret-down"></i>' + trees[x]['label'] + '</div></div>');
      } else {
        var treeContainer = $('<div class="pt-tree-container pt-open"></div>');
      }
      if (openTree == trees[x]['id']) {
        treeContainer.addClass('pt-open');
      }
      treeContainer.on('click', function (e) {
        $('.pt-tree-container').removeClass('pt-open');
        $(this).addClass('pt-open');
        localStorage.setItem("pt-open-tree", $(this).find('.pt-pagetree').data('menu-id'));
      });
      $('.pt-trees-wrapper').append(treeContainer);
      var tree = $('<ul class="pt-pagetree" data-menu-id="' + trees[x]['id'] + '"></ul>');
      $(treeContainer).append(tree);
      augmentData(trees[x]['tree']);
      var menuLanguage = defaultLang;
      if (trees[x]['langcode']) {
        menuLanguage = trees[x]['langcode'];
      }
      buildTree(trees[x]['tree'], tree, currentNode, menuLanguage);
      makeSortable(tree);
    }
  }

  function augmentData(list) {
    var hasCurrent = false;
    for (var i in list) {
      if (list[i].children.length > 0) {
        var current = augmentData(list[i].children);
      }
      if (list[i].id == currentNode || current || openElements.hasOwnProperty(list[i].id)) {
        list[i].open = true;
        hasCurrent = true;
      }
    }
    return hasCurrent;
  }

  function openSubtree(id, element) {
    return function (e) {
      $(this).toggleClass("fa-plus").toggleClass('fa-minus');
      if ($(this).hasClass("fa-plus")) {
        delete openElements[id];
      } else {
        openElements[id] = true;
      }
      element.toggleClass("pt-open");
      localStorage.setItem('pt-states', JSON.stringify(openElements));
    }
  }

  function buildElement(entry, language) {
    var element = $('<li class="pt-entry pt-main-entry pt-entry-' + entry.id + '-' + language + '" data-node-id="' + entry.id + '" data-link-id="' + entry.linkId + '" id="list_item_' + entry.id + '"></li>');
    return element;
  }

  function buildTranslation(entry, language) {
    var translationEntry = $('<li class="pt-entry pt-entry-' + entry.id + '-' + language + '"></li>');
    var translationContainer = $("<div></div>");
    if (entry['translations'][language]['status'] > -1) {
      var translationLink = $('<a title="' + entry['translations'][language]['name'] + ' [' + language + ']" href="' + entry['translations'][language]['externalLink'] + '">' + entry['translations'][language]['name'] + ' [' + language + ']</a>');
    } else {
      var translationLink = $('<span>' + Drupal.t('No translation exists') + ' [' + language + ']</span>');
    }
    translationContainer.append(translationLink);
    addActions(translationContainer, entry, language);
    addFeedback(translationContainer);
    if (currentLanguage == language) {
      translationEntry.addClass("pt-current");
    }
    if (entry['translations'][language]['status'] > -1) {
      setState(translationLink, entry['translations'][language]['status']);
    }
    translationEntry.append(translationContainer);
    return translationEntry;
  }

  function setState(element, status) {
    if (element.find('i').length == 0) {
      element.prepend('<i class="pt-state">');
    }
    if (status == 0) {
      element.find('i').addClass("fas fa-times-circle");
    } else if (status == 2) {
      element.find('i').addClass("fas fa-exclamation-circle");
    } else {
      element.find('i').addClass("fas fa-check-circle");
    }
  }

  function buildTree(list, parent, currentNode, mainLanguage) {
    parent.empty();
    for (var x in list) {
      var element = buildElement(list[x], mainLanguage);
      var container = $("<div></div>");


      if (list[x]['translations'][mainLanguage] && typeof list[x]['translations'][mainLanguage] != 'undefined') {
        var link = $('<a title="' + list[x]['translations'][mainLanguage]['name'] + ' [' + mainLanguage + ']" href="' + list[x]['translations'][mainLanguage]['externalLink'] + '">' + list[x]['translations'][mainLanguage]['name'] + ' [' + mainLanguage + ']</a>');

        setState(link, list[x]['translations'][mainLanguage]['status']);
      } else {
        link = $('<a href="/' + mainLanguage + '/node/' + list[x]['id'] + '">' + Drupal.t('No translation exists') + ' [' + mainLanguage + ']</a>');
      }
      container.append(link);

      addActions(container, list[x], mainLanguage);
      addFeedback(container);
      if (list[x].children.length > 0) {
        var opener = $('<div class="pt-opener far fa-plus"></div>');
        if (list[x].children.length > 0 && list[x]['open']) {
          opener.removeClass("fa-plus").addClass('fa-minus');
          element.addClass('pt-open');
        }
        opener.on("click", openSubtree(list[x].id, element));
        container.append(opener);
      }

      if (list[x].id == currentNode) {
        if (currentLanguage == mainLanguage) {
          element.addClass("pt-current");
        }
        var translations = $('<ul class="pt-translations"></ul>');
        for (var language in list[x]['translations']) {
          if (language != mainLanguage) {
            translationEntry = buildTranslation(list[x], language)
            translations.append(translationEntry);
          }
        }
        container.append(translations);
      }
      element.append(container);
      parent.append(element);
      var subtree = $('<ul class="pt-subtree"></ul>');
      element.append(subtree);
      if (list[x].children.length > 0) {
        buildTree(list[x].children, subtree, currentNode, mainLanguage);
      }
    }
  }

  function addActions(element, entry, language) {
    var actionList = buildActions(entry, language);
    if (actionList.children().length > 0) {
      var actions = $('<i class="pt-icon fa fa-bars"></i>');
      actions.on("click", function () {
        var hidden = $(this).next().is(":hidden");
        $(".pt-pagetree .pt-page-actions").hide();
        if (hidden) {
          $(this).next().show();
        }
      });
      actionList.hide();
      element.append(actions);
      element.append(actionList);

      actionList.on('click', 'li', function (event) { $(this).parent().addClass('hide'); event.stopPropagation(); });
      actionList.on('mousedown, mouseup, mousemove', 'li', function (event) { event.stopPropagation(); });

      actions.on('mousedown, mouseup, mousemove', function (event) { event.stopPropagation(); });
    }
  }

  function addFeedback(element, entry, language) {
    var feedback = $('<div class="pt-feedback"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i><i class="fa fa-check"></i><i class="fa fa-exclamation"></i></div>');
    element.append(feedback);
  }

  function buildActions(entry, language) {
    var actionList = $('<ul class="pt-page-actions hide"></ul>');

    if (entry['translations'][language] && typeof entry['translations'][language] != 'undefined') {
      actions = entry['translations'][language].actions;
      for (var x in actions) {
        if (typeof actions[x].link != 'undefined') {
          var actionListItem = $('<li></li>');
          var action = $('<a href="' + actions[x].link + '">' + Drupal.t(actions[x].label) + '</a>');

          actionListItem.append(action);
          actionList.append(actionListItem);
        } else if (typeof actions[x].callback != 'undefined') {
          var actionListItem = $('<li></li>');
          var action = $('<a>' + Drupal.t(actions[x].label) + '</a>');
          action.on('click',
            (function (callback, args) {
              return function (e) {
                applyNamespaced(callback, args);
              };
            })(actions[x].callback, actions[x].arguments)
          );
          actionListItem.append(action);
          actionList.append(actionListItem);
        }
      }
    }
    return actionList;
  }
  function copyNodeSingle(nodeId, parentId, position, menu) {
    var entry = $('.pt-main-entry[data-node-id=' + nodeId + ']');
    entry.addClass("pt-saving");
    Drupal.restconsumer
      .post('/frontendpublishing/copy', { id: nodeId, newParent: parentId, weight: position, recursive: false, menu: menu })
      .done(
        (function (entry) {
          return function (data) {
            loading = true;
            var hash = sessionStorage.getItem("pt-tree-hash");
            Drupal.restconsumer.get('/pagetree/tree/' + hash).done(
              (function (cloneId) {
                return function (data) {
                  var entry = $('.pt-main-entry[data-node-id=' + cloneId + ']');
                  entry.removeClass('pt-saving').addClass('pt-saved');
                  setTimeout(
                    (function (entry) {
                      return function () {
                        entry.removeClass('pt-saved');
                        if (data.hash !== hash) {
                          sessionStorage.setItem("pt-tree-hash", data.hash);
                          createTree(data, currentNode);
                        }
                        loading = false;
                      }
                    })(entry)
                    , 1500);
                }
              })(data['clone'])
            );
          }
        })(entry)
      )
      .fail(function (e) {
        loading = true;
        var hash = sessionStorage.getItem("pt-tree-hash");
        Drupal.restconsumer.get('/pagetree/tree/' + hash).done(
          function (data) {
            if (data.hash !== hash) {
              sessionStorage.setItem("pt-tree-hash", data.hash);
              createTree(data, currentNode);
            }
            loading = false;
          }
        );
      });
  }

  function copyNodeRecursive(nodeId, parentId, position, menu) {
    var entry = $('.pt-main-entry[data-node-id=' + nodeId + ']');
    entry.addClass("pt-saving");
    Drupal.restconsumer
      .post('/frontendpublishing/copy', { id: nodeId, newParent: parentId, weight: position, recursive: true, menu: menu })
      .done(
        (function (entry) {
          return function (data) {
            loading = true;
            var hash = sessionStorage.getItem("pt-tree-hash");
            Drupal.restconsumer.get('/pagetree/tree/' + hash).done(
              (function (cloneId) {
                return function (data) {
                  var entry = $('.pt-main-entry[data-node-id=' + cloneId + ']');
                  entry.removeClass('pt-saving').addClass('pt-saved');
                  setTimeout(
                    (function (entry) {
                      return function () {
                        entry.removeClass('pt-saved');
                        if (data.hash !== hash) {
                          sessionStorage.setItem("pt-tree-hash", data.hash);
                          createTree(data, currentNode);
                        }
                        loading = false;
                      }
                    })(entry)
                    , 1500);
                }
              })(data['clone'])
            );
          }
        })(entry)
      )
      .fail(function (e) {
        loading = true;
        var hash = sessionStorage.getItem("pt-tree-hash");
        Drupal.restconsumer.get('/pagetree/tree/' + hash).done(
          function (data) {
            if (data.hash !== hash) {
              sessionStorage.setItem("pt-tree-hash", data.hash);
              createTree(data, currentNode);
            }
            loading = false;
          }
        );
      });
  }

  function makeSortable(tree) {
    if (tree.hasClass('ui-sortable')) {
      tree.nestedSortable('refresh');
    }
    tree.nestedSortable({
      items: "li.pt-main-entry",
      toleranceElement: '>div',
      listType: 'ul',
      handle: '>div>a',
      protectRoot: false,
      helper: function (e, t) {
        if (e.shiftKey) {
          var copyHelper = t.clone();
          copyHelper.insertAfter(t);
          this.copy = true;
          t.item = copyHelper
        }
        return t;
      },
      relocate: function (e, t) {
        if (t.item.attr('cancel') != '1') {
          var position = t.item.prevAll('li').length + 1;
          var nid = t.item.attr("data-node-id");
          var lid = t.item.attr("data-link-id");
          var parentId = t.item.parent().parent().attr("data-link-id");
          var menu = t.item.closest('.pt-pagetree').attr("data-menu-id");
          if (typeof parentId == 'undefined') {
            parentId = -1;
          }
          if (this.copy) {
            t.item.attr("data-copy-item", 1);
            var dialogButtons = {};
            dialogButtons[Drupal.t('Exclude children')] = function () {
              $(this).dialog("close");
              copyNodeSingle(nid, parentId, position, menu);
            };
            dialogButtons[Drupal.t('Include children')] = function () {
              $(this).dialog("close");
              copyNodeRecursive(nid, parentId, position, menu);
            };
            dialogButtons[Drupal.t('Cancel')] = function () { $(this).dialog("close"); };
            $('#ptCopyModal').dialog({
              resizable: false,
              height: "auto",
              width: 400,
              modal: true,
              buttons: dialogButtons
            });
          } else {
            t.item.addClass('pt-saving');
            Drupal.restconsumer
              .patch('/frontendpublishing/move', { id: lid, newParent: parentId, weight: position, menu: menu })
              .done(
                (function (entry) {
                  return function (e) {
                    loading = false;
                    var hash = sessionStorage.getItem("pt-tree-hash");
                    Drupal.restconsumer.get('/pagetree/tree/' + hash).done(function (data) {
                      entry.removeClass('pt-saving').addClass('pt-saved');
                      setTimeout(
                        (function (entry) {
                          return function () {
                            entry.removeClass('pt-saved');
                            if (data.hash !== hash) {
                              sessionStorage.setItem("pt-tree-hash", data.hash);
                              createTree(data, currentNode);
                            }
                            loading = false;
                          }
                        })(entry)
                        , 300);
                    });
                  }
                })(t.item)
              );
          }
        }
        t.item.attr('cancel', '0');
        this.copy = false;
      }
    });
    // Cancel sorting on esc key.
    $(document).keydown(function (e) {
      if (e.keyCode == 27 && tree.hasClass('ui-sortable')) {
        $('.ui-sortable-helper').attr('cancel', '1');
        tree.sortable("cancel");
      }
    });
  }

  function applyNamespaced(str, args, parent) {
    if (str.indexOf('.') == -1) {
      if (typeof parent == 'undefined') {
        window[str].apply(window, args);
      } else {
        parent[str].apply(parent, args);
      }
      return;
    }
    var parts = str.split('.', 1);
    if (typeof parent == 'undefined') {
      parent = window[parts[0]];
    } else {
      parent = parent[parts[0]];
    }
    str = str.replace(parts[0] + '.', '');
    applyNamespaced(str, args, parent);
    return;
  }

  $(window).on('load', function () {
    var items = $('.pt-main-entry');
    items.addClass('enable-sorting');
    $('.pt-pagetree').each(function (i, tree) {
      $(tree).nestedSortable('refresh');
    });
  });

  $(window).on('frontendpublishing_transition_start', function (e, nodeId, lang, state, message) {
    var entry = jQuery('.pt-entry-' + nodeId + '-' + lang);
    entry.addClass("pt-saving");
  });

  $(window).on('frontendpublishing_transition_done', function (e, nodeId, lang, state, message) {
    var entry = jQuery('.pt-entry-' + nodeId + '-' + lang);
    entry.removeClass('pt-saving').addClass('pt-saved');
    setTimeout(
      (function (entry) {
        return function () {
          entry.removeClass('pt-saved');
        }
      })(entry)
      , 1500);
    if (state == 'publish') {
      entry.find('.pt-state:first').removeClass('fa-exclamation-circle').removeClass('fa-times-circle').addClass('fa-check-circle');
    } else if (state == 'unpublish') {
      entry.find('.pt-state:first').removeClass('fa-exclamation-circle').removeClass('fa-check-circle').addClass('fa-times-circle');
    }
  });


  $(window).on('frontendpublishing_transition_fail', function (e, nodeId, lang, state, message) {
    var entry = jQuery('.pt-entry-' + nodeId + '-' + lang);
    entry.addClass('pt-error');
  });
})(jQuery, Drupal);
