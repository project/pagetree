<?php

namespace Drupal\pagetree\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a reusable pagetree state plugin annotation object.
 *
 * @Annotation
 */
class StateHandler extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the state plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The weight of the state plugin.
   *
   * @var int
   */
  public $weight = 100;

}
