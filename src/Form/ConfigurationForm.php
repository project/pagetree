<?php

namespace Drupal\pagetree\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Page tree configuration form.
 *
 * @package Drupal\pagetree\Form
 */
class ConfigurationForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entitiyTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * ConfigurationForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager service.
   */
  public function __construct(ConfigFactory $config, EntityTypeManager $entity_type_manager, LanguageManager $language_manager) {
    $this->config = $config;
    $this->entitiyTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pagetree.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'pagetree';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $this->config($this->getEditableConfigNames()[0]);

    $form['general'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('General settings'),
      '#description' => $this->t('Set the general settings for the page tree modules.'),
    ];

    // Let the user pick the menus to display.
    $menus = $this->entitiyTypeManager->getStorage('menu')->loadMultiple();
    $menusList = [];
    foreach ($menus as $menu) {
      $menusList[$menu->id()] = $menu->label();
    }
    asort($menusList);

    $form['general']['menus'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Menus'),
      '#description' => $this->t('Select the menus to be displayed in the page tree.'),
      '#options' => $menusList,
      '#default_value' => $values->get('menus'),
    ];

    // Let the user pick the languages to display.
    $languages = $this->languageManager->getLanguages();
    $languagesList = [];
    foreach ($languages as $language) {
      $languagesList[$language->getId()] = $language->getName();
    }
    asort($languagesList);

    $form['general']['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages'),
      '#description' => $this->t('Select the languages to be displayed in the page tree.'),
      '#options' => $languagesList,
      '#default_value' => $values->get('languages'),
    ];

    // Let the user pick the content types to display.
    $contentTypes = $this->entitiyTypeManager->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    $form['general']['contenttypes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#description' => $this->t('Select the content types you want to process with the page tree module.'),
      '#options' => $contentTypesList,
      '#default_value' => $values->get('contentTypes'),
    ];

    $form['general']['reset_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set unpublished?'),
      '#description' => $this->t('If checked, the publication status of the clone will be unpublished.'),
      '#default_value' => $values->get('reset_status'),
    ];

    $titleSourceList = [
      'node' => $this->t('Node'),
      'menu_link' => $this->t('Menu link'),
    ];
    $form['general']['title_source'] = [
      '#type' => 'select',
      '#title' => 'Pagetree title source',
      '#description' => $this->t('Select the source of the titles, it can be taken from the node or menu links.'),
      '#options' => $titleSourceList,
      '#default_value' => !empty($values->get('title_source')) ? $values->get('title_source') : 'node',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Store set values.
    $this->config->getEditable(self::getEditableConfigNames()[0])
      ->set('menus', array_filter($form_state->getValue('menus')))
      ->set('languages', array_filter($form_state->getValue('languages')))
      ->set('contentTypes', array_filter($form_state->getValue('contenttypes')))
      ->set('reset_status', $form_state->getValue('reset_status'))
      ->set('title_source', $form_state->getValue('title_source'))
      ->save();
  }

}
