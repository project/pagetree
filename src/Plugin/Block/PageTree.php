<?php

namespace Drupal\pagetree\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an page tree block.
 *
 * The block needs to be added to the themes header region to take effect.
 *
 * @Block(
 *   id = "pagetree_block",
 *   admin_label = @Translation("Page Tree Block"),
 * )
 */
class PageTree extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a PageTree Block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Routing\AdminContext $adminRouteContext
   *   The admin route context.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The langauge manager.
   */
  public function __construct(
    array $configuration,
  string $plugin_id,
  $plugin_definition,
  protected AccountInterface $currentUser,
  protected AdminContext $adminRouteContext,
  protected LanguageManager $languageManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('router.admin_context'),
      $container->get('language_manager'),
    );
  }

  /**
   * Builds the page tree block.
   *
   * @return array
   *   The render array.
   */
  public function build() {
    $build = [];

    // If user is logged in and has permission to edit page nodes,
    // enable edit mode.
    if ($this->currentUser->hasPermission('use pagetree')) {
      // Attach library for editing.
      $build['#attached']['library'] = ['pagetree/pagetree'];

      // Set values for displaying page tree.
      $build['#theme'] = 'pagetree';
      $build['#attached']['drupalSettings']['pagetree']['defaultLanguage'] = $this->languageManager->getDefaultLanguage()->getId();
    }
    // Cache per route and permissions.
    $build['#cache'] = [
      'contexts' => ['user.permissions'],
    ];

    return $build;
  }

}
