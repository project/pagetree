<?php

namespace Drupal\pagetree\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\pagetree\Plugin\pagetree\StatePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract page tree state plugin implementation as a base class for extension.
 */
abstract class StatePluginBase extends PluginBase implements StatePluginInterface {

  /**
   * The node bundle information.
   *
   * @var array
   */
  protected static $bundleInfo = [];

  /**
   * Create a new page tree state plugin.
   *
   * Provides the node bundle information in self::$bundleInfo
   * for extending classes to consume.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   Definitions for the plugin.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if (empty(self::$bundleInfo)) {
      self::$bundleInfo = \Drupal::service("entity_type.bundle.info")->getAllBundleInfo()['node'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition
      );
  }

  /**
   * Empty implementation of the annotate function.
   *
   * @param array $entries
   *   The entries of the page tree (inout).
   */
  public function annotate(array &$entries) {

  }

  /**
   * Empty implementation of the alterEntry function.
   *
   * @param array $entry
   *   An entry in the page tree (inout).
   */
  public function alterEntry(array &$entry) {
  }

}
