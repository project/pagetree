<?php

namespace Drupal\pagetree\Plugin\frontendpublishing\StateChange;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\frontendpublishing\Plugin\StateChangePluginBase;

/**
 * Adds the status to the cloned entity on copy.
 *
 * @StateChangeHandler(
 *   id = "pagetree",
 *   name = @Translation("Pagetree state change handler"),
 *   weight = 1000
 * )
 */
class Standard extends StateChangePluginBase {

  /**
   * {@inheritdoc}
   */
  public function copy(ContentEntityBase &$entity, ContentEntityBase &$clone = NULL) {
    $reset_status = \Drupal::configFactory()->get('pagetree.settings')->get('reset_status');
    if ($reset_status) {
      foreach ($clone->getTranslationLanguages(TRUE) as $language) {
        $clone->getTranslation($language->getId())->setPublished(FALSE);
      }
    }
  }

}
