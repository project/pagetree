<?php

namespace Drupal\pagetree\Plugin\pagetree\State;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pagetree\Plugin\StatePluginBase;

/**
 * Adds the default actions to page tree entries.
 *
 * @StateHandler(
 *   id = "standard",
 *   name = @Translation("Default pagetree state handler"),
 *   weight = 100
 * )
 */
class Standard extends StatePluginBase {

  use StringTranslationTrait;

  /**
   * Alter the entries to include the default actions.
   *
   * Add create, edit form, publish, unpublish and delete actions.
   * The actions are added depending on the entity type permissions.
   *
   * @param array $entry
   *   An entry in the page tree (inout).
   */
  public function alterEntry(array &$entry) {
    parent::alterEntry($entry);
    $isMultilingual = \Drupal::languageManager()->isMultilingual();
    foreach ($entry['translations'] as $language => &$translation) {
      $addLink = '/node/add/' . $entry['bundle'] . '?parentEntry=' . $entry['linkId'];
      $settingsLink = '/node/' . $entry['id'] . '/edit';
      $addTranslationLink = '/node/' . $entry['id'] . '/translations';
      $deleteLink = '/node/' . $entry['id'] . '/delete';
      if ($translation['status'] == -1) {
        if ($isMultilingual && $entry['permissions']['update']) {
          $translation['actions'][] =
          [
            'link' => '/' . $language . $addTranslationLink,
            'label' => $this->t('Add translation'),
            'weight' => 0,
          ];
        }
      }
      else {
        if ($isMultilingual) {
          $settingsLink = '/' . $language . $settingsLink;
          $addLink = '/' . $language . $addLink;
        }
        if ($translation['defaultTranslation'] != $language) {
          $deleteLink = '/' . $language . $deleteLink;
        }
        if ($entry['permissions']['create']) {
          $translation['actions'][] =
          [
            'link' => $addLink,
            'label' => $this->t('Add @bundle', ['@bundle' => self::$bundleInfo[$entry['bundle']]['label']]),
            'weight' => 0,
          ];
        }
        if ($entry['permissions']['update']) {
          $translation['actions'][] =
          [
            'link' => $settingsLink,
            'label' => $this->t('Settings'),
            'weight' => 500,
          ];
          $translation['actions'][] = [
            'label' => $this->t('Publish'),
            'callback' => 'Drupal.frontendpublishing.showTransitionDialog',
            'arguments' => [$entry['id'], $language, TRUE],
            'weight' => 1000,
          ];
          $translation['actions'][] = [
            'label' => $this->t('Unpublish'),
            'callback' => 'Drupal.frontendpublishing.showTransitionDialog',
            'arguments' => [$entry['id'], $language, FALSE],
            'weight' => 1500,
          ];
        }
        if ($entry['permissions']['delete']) {
          $translation['actions'][] = [
            'link' => $deleteLink,
            'label' => $this->t('Delete'),
            'weight' => 2000,
          ];
        }
      }
    }
  }

}
