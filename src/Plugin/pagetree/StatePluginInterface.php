<?php

namespace Drupal\pagetree\Plugin\pagetree;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Page tree state plugin interface.
 */
interface StatePluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Alter entry information before they are converted to an array tree.
   *
   * @param array $entries
   *   The entries of the page tree (inout).
   */
  public function annotate(array &$entries);

  /**
   * Alter entry information while they are converted to an array tree.
   *
   * @param array $entry
   *   An entry in the page tree (inout).
   */
  public function alterEntry(array &$entry);

}
