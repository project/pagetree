<?php

namespace Drupal\pagetree\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get the page tree.
 *
 * @RestResource(
 *   id = "pagetree_tree",
 *   label = @Translation("Get the tree"),
 *   uri_paths = {
 *     "canonical" = "/pagetree/tree/{hash}"
 *   }
 * )
 */
class Tree extends ResourceBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new TreeResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('pagetree'),
          $container->get('current_user')
      );
  }

  /**
   * Request the current page tree.
   *
   * @param string $hash
   *   The hash of the last build.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the page tree.
   */
  public function get($hash = NULL) {

    if (!$this->currentUser->hasPermission('use pagetree')) {
      throw new AccessDeniedHttpException('You are not allowed to see the page tree.');
    }
    $data = \Drupal::service('pagetree.tree')->get();
    if (empty($data)) {
      $response = new ResourceResponse($data);
      $response->addCacheableDependency(['#cache' => ['max-age' => 0]]);
    }
    else {
      $cache = $data['#cache'];
      unset($data['#cache']);
      if ($data['hash'] == $hash) {
        $response = new ResourceResponse(['hash' => $hash]);
      }
      else {
        $response = new ResourceResponse($data);
      }
      $response->addCacheableDependency($cache);
    }
    return $response;
  }

}
