<?php

namespace Drupal\pagetree\Service;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\frontendpublishing\Service\MenuHelper;
use Drupal\node\Entity\Node;
use Drupal\path_alias\AliasManager;

/**
 * Provides a resource to get the page tree.
 */
class Tree {

  /**
   * The content types to display.
   *
   * @var string[]
   */
  protected $contentTypes = [];

  /**
   * The languages to be displayed.
   *
   * @var \Drupal\Core\Language\LanguageInterface[]
   */
  protected $languagesUsed = [];

  /**
   * The handlers to process entries.
   *
   * @var \Drupal\pagetree\Plugin\pagetree\StatePluginInterface[]
   */
  protected $handlers = [];

  /**
   * List of handled ids.
   *
   * @var int[]
   */
  protected $handledIds = [];

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $cache = NULL;

  /**
   * The config.
   *
   * @var \Drupal\config
   */
  protected $config;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * State manager.
   *
   * @var \Drupal\pagetree\Service\StatePluginManager
   */
  protected $stateManager;

  /**
   * The frontendpublishing menu helper.
   *
   * @var \Drupal\frontendpublishing\Service\MenuHelper
   */
  protected $menuHelper;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entitiyTypeManager;

  /**
   * The path alias manager service.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $pathAliasManager;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Create a new tree service.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager service.
   * @param \Drupal\pagetree\Service\StatePluginManager $state_manager
   *   State manager.
   * @param \Drupal\frontendpublishing\Service\MenuHelper $menu_helper
   *   The frontendpublishing menu helper.
   * @param \Drupal\Core\Entity\EntityTypeManager $entitiy_type_manager
   *   The entity type manager service.
   * @param \Drupal\path_alias\AliasManager $path_alias_manager
   *   The path alias manager service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(
    ConfigFactory $config,
    LanguageManager $language_manager,
    StatePluginManager $state_manager,
    MenuHelper $menu_helper,
    EntityTypeManager $entitiy_type_manager,
    AliasManager $path_alias_manager,
    Connection $database
  ) {
    $this->config = $config->get('pagetree.settings');
    $this->languageManager = $language_manager;
    $this->stateManager = $state_manager;
    $this->menuHelper = $menu_helper;
    $this->entitiyTypeManager = $entitiy_type_manager;
    $this->pathAliasManager = $path_alias_manager;
    $this->database = $database;

    $this->contentTypes = $this->config->get('contentTypes');
    $languagesSelected = $this->config->get('languages');
    $this->languagesUsed = [];
    $languages = $this->languageManager->getLanguages();
    $this->languagesUsed = [];
    if ($languagesSelected != NULL && (is_countable($languagesSelected) ? count($languagesSelected) : 0) > 0) {
      foreach ($languages as $language) {
        if (in_array($language->getId(), $languagesSelected)) {
          $this->languagesUsed[] = $language;
        }
      }
    }
    else {
      $this->languagesUsed = $languages;
    }
    $this->handlers = $this->stateManager->getHandlers();
    $this->cache = new CacheableMetadata();
  }

  /**
   * Request the current page tree.
   *
   * @return array
   *   The prepared array tree
   */
  public function get() {
    // Get nids in the menus selected in the configuration.
    $menus = $this->config->get('menus');
    $nids = [];
    $menuTrees = [];
    foreach ($menus as $menuId) {
      $menuTrees[$menuId] = $this->menuHelper::getMenuTree($menuId);
      $this->collectNids($menuTrees[$menuId], $nids);
    }
    if ((is_countable($nids) ? count($nids) : 0) == 0) {
      return [];
    }

    // Get node and revision information, build the entity information array.
    $nodes = $this->getNodes($nids);
    $revisionData = $this->getRevisionData($nids);
    foreach ($this->handlers as $handler) {
      $handler->annotate($revisionData);
    }
    $entityInfos = [];
    $this->processNodes($nodes, $revisionData, $entityInfos);

    // Generate array trees for frontend display.
    $trees = [];
    foreach ($menus as $menuId) {
      $menu = $this->entitiyTypeManager->getStorage('menu')->load($menuId);
      if ($menu != NULL) {
        // Combine the info.
        $arrayTree = $this->buildArrayTree($menuTrees[$menuId], $entityInfos);
        $trees[] = [
          'label' => $menu->label(),
          'id' => $menu->id(),
          'langcode' => $menu->language()->getId(),
          'tree' => $arrayTree,
        ];
      }
    }
    $this->cache->addCacheableDependency(
      CacheableMetadata::createFromRenderArray(
        [
          '#cache' =>
          [
            'tags' => [
              'menu_link_content_list',
            ],
            'contexts' => [
              'user.permissions',
              'user.node_grants:view',
            ],
          ],
        ]
      )
    );
    $data = [
      'hash' => md5(serialize($trees)),
      'trees' => $trees,
      '#cache' => $this->cache,
    ];
    return $data;
  }

  /**
   * Process the nodes to display.
   *
   * Use the node and revision information to collect all necessary information
   * for the pagetree.
   *
   * @param array $nodes
   *   The nodes to display.
   * @param array $revisionData
   *   The revision information.
   * @param array $entityInfos
   *   The resulting entity information (inout).
   */
  protected function processNodes(array &$nodes, array &$revisionData, array &$entityInfos) {

    $title_source = $this->config->get('title_source');
    foreach ($this->languagesUsed as &$language) {
      if (!empty($nodes[$language->getId()])) {
        foreach ($nodes[$language->getId()] as $node) {
          $this->cache->addCacheableDependency($node);
          if (empty($entityInfos[$node->id()])) {
            $entityInfos[$node->id()] =
              [
                'bundle' => $node->getType(),
                'translations' => [],
                'permissions' => $this->getPermissions($node),
              ];
          }
          if (!empty($revisionData[$node->id() . $language->getId()])) {
            $revision = $revisionData[$node->id() . $language->getId()];
            if (!empty($revision['#cache'])) {
              $this->cache->addCacheableDependency(CacheableMetadata::createFromRenderArray($revision));
            }
            $status = 0;
            $translation = [];
            if ($node->status->value == 1) {
              if ($revision['status'] == 1) {
                $status = 1;
              }
              else {
                $status = 2;
              }
            }
            $externalLink = $this->pathAliasManager->getAliasByPath('/node/' . $node->id(), $language->getId());
            $settingsLink = '/node/' . $node->id() . '/edit';
            if ($this->languageManager->isMultilingual()) {
              $externalLink = '/' . $language->getId() . $externalLink;
              $settingsLink = '/' . $language->getId() . $settingsLink;
            }

            $translation['name'] = $title_source == 'menu_link' && !empty($revision['title']) ? $revision['title'] : $revision['label'];
            $translation['status'] = $status;
            $translation['externalLink'] = $externalLink;
            $translation['nodeId'] = $node->id();
            $translation['language'] = $language->getId();
            $translation['defaultTranslation'] = $node->getUntranslated()->language()->getId();
            $entityInfos[$node->id()]['translations'][$language->getId()] = $translation;
          }
          else {
            $entityInfos[$node->id()]['translations'][$language->getId()] =
              [
                'name' => 'No translation exists.',
                'status' => -1,
              ];
          }
        }
      }
    }
    foreach ($entityInfos as $nodeId => $node) {
      foreach ($this->languagesUsed as &$language) {
        if (empty($entityInfos[$nodeId]['translations'][$language->getId()])) {
          $entityInfos[$nodeId]['translations'][$language->getId()] =
            [
              'name' => 'No translation exists.',
              'status' => -1,
            ];
        }
      }
    }
  }

  /**
   * Collect the node information based on the given node ids.
   *
   * @param array $nids
   *   The node ids to load the information for.
   *
   * @return array
   *   The node information.
   */
  public function getNodes(array $nids) {
    $nodes = $this->entitiyTypeManager->getStorage('node')->loadMultiple($nids);
    $return = [];
    /** @var \Drupal\Core\Entity\ContentEntityInterface $node */
    foreach ($nodes as $node) {
      $languages = $node->getTranslationLanguages(TRUE);
      foreach ($languages as $language) {
        $return[$language->getId()][] = $node;
      }
    }
    return $return;
  }

  /**
   * Collect the revision information based on the given node IDs.
   *
   * @param array $nids
   *   The node ids.
   *
   * @return array
   *   The revision information.
   */
  public function getRevisionData(array $nids) {
    $revisionData = [];
    foreach ($this->languagesUsed as &$language) {
      $vids = $this->getRevisionIds($nids, $language);
      $query = $this->database->select('node_field_revision', 'nfr');
      if (empty($vids)) {
        break;
      }
      $query->addField('nfr', 'title', 'label');
      $query->addField('nfr', 'nid');
      $query->addField('nfr', 'status');
      $query->addField('nfr', 'vid');
      $query->condition('nfr.vid', $vids, 'IN');
      $query->condition('nfr.langcode', $language->getId());
      $query->addJoin('INNER', 'menu_link_content_data', 'mlcd', "(mlcd.link__uri = CONCAT('entity:node/', nfr.nid) OR link__uri = CONCAT('internal:/node/', nfr.nid)) AND  mlcd.langcode = :langcode", [':langcode' => $language->getId()]);
      $query->addField('mlcd', 'title');
      $stmt = $query->execute();
      while ($row = $stmt->fetch()) {
        $revisionData[$row->nid . $language->getId()] =
          [
            'label' => $row->label,
            'nid' => $row->nid,
            'langcode' => $language->getId(),
            'title' => $row->title,
            'status' => $row->status,
            'vid' => $row->vid,
            '#cache' => [],
          ];
      }
    }
    return $revisionData;
  }

  /**
   * Return the newest revision id for each given node id per language.
   *
   * @param array $nids
   *   The node ids.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language.
   *
   * @return array
   *   The revision ids.
   */
  public function getRevisionIds(array $nids, LanguageInterface $language) {
    $vids = [];
    $subquery = $this->database->select('node_field_revision', 'snfr');
    $subquery->addExpression('MAX(snfr.vid)', 'vid');
    $subquery->condition('snfr.nid', $nids, 'IN');
    $subquery->condition('snfr.langcode', $language->getId(), 'LIKE');
    $subquery->groupBy('snfr.nid');
    $subquery->orderBy('snfr.changed', 'DESC');
    $subquery->execute();
    $stmt = $subquery->execute();
    while ($row = $stmt->fetch()) {
      $vids[] = $row->vid;
    }
    return $vids;
  }

  /**
   * Create a array tree representation of the menu based on the given menutree.
   *
   * Annotate it with info from entityInfos.
   *
   * @param \Drupal\system\Entity\MenuLinkTreeInterface[] $tree
   *   The menu tree.
   * @param array[] $entityInfos
   *   The entity information.
   *
   * @return array
   *   The entity information as a tree.
   */
  public function buildArrayTree(array &$tree, array &$entityInfos) {
    $return = [];
    foreach ($tree as $entry) {
      if (array_key_exists('node', $entry->link->getRouteParameters())) {
        $id = $entry->link->getRouteParameters()['node'];
        if (isset($entityInfos[$id])) {
          $linkId = str_replace('menu_link_content:', '', (string) $entry->link->getPluginId());
          $output = [
            'id' => $id,
            'bundle' => $entityInfos[$id]['bundle'],
            'linkId' => $linkId,
            'translations' => &$entityInfos[$id]['translations'],
            'permissions' => &$entityInfos[$id]['permissions'],
          ];

          if (!in_array($id, $this->handledIds)) {
            // Allow handlers to alter the entries.
            foreach ($this->handlers as $handler) {
              $handler->alterEntry($output);
            }
            // Sort actions by weight.
            foreach ($output['translations'] as &$translation) {
              if ($translation != NULL && !empty($translation['actions'])) {
                usort(
                  $translation['actions'],
                  ['\\' . SortArray::class, 'sortByWeightElement']
                );
              }
            }
            $this->handledIds[] = $id;
          }

          $output['children'] = $this->buildArrayTree($entry->subtree, $entityInfos);
          $return[] = $output;
        }
      }
    }
    return $return;
  }

  /**
   * Collect all the node ids from a menu tree.
   *
   * @param \Drupal\system\Entity\MenuLinkTreeInterface[] $tree
   *   The tree to parse.
   * @param array $nids
   *   The node ids (inout).
   *
   * @return array
   *   The node ids;
   */
  public function collectNids(array $tree, array &$nids = []) {
    foreach ($tree as $entry) {
      if (array_key_exists('node', $entry->link->getRouteParameters())) {
        $nid = $entry->link->getRouteParameters()['node'];
        $nids[] = $nid;
        $this->collectNids($entry->subtree, $nids);
      }
    }
    return $nids;
  }

  /**
   * Get the permissions for the current user.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The bundle to collect the permissions for.
   *
   * @return array
   *   The permissions for the given bundle and the current user.
   */
  public function getPermissions(Node $node) {
    $permissions = [];
    $permissions['create'] = $node->access('create');
    $permissions['update'] = $node->access('update');
    $permissions['delete'] = $node->access('delete');
    return $permissions;
  }

}
